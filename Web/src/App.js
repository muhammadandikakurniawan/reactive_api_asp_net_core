import logo from './logo.svg';
import './App.css';
import React from 'react';
import * as signalR from '@microsoft/signalr'

class App extends React.Component{

  constructor(props){
    super(props)

    const connection = new signalR.HubConnectionBuilder()
    .withUrl("https://localhost:5001/hubs/chat")
    .build();

    this.state = {
      UserId : "dika-001",
      SignalRConnection :  connection
    }





    this.state.SignalRConnection.on(this.state.UserId, (sender, receiver, message) => {

      console.log("Sender : "+sender);
      console.log("receiver : "+receiver);
      console.log("message : "+message);

      const recMessage = sender + ": " + message;
      const li = document.createElement("li");
  
      li.textContent = recMessage;
      document.getElementById("messageList").appendChild(li);
    });

    this.state.SignalRConnection.start()
    .then((p) => {
        console.info('SignalR Connected'); console.log(p)
    })
    .catch(err =>{
    console.log('SignalR Connection Error: ', err)});
  }

  
  sendMessage = () => {
    var sender = document.querySelector("#sender").value
    var receiver = document.querySelector("#receiver").value
    var message = document.querySelector("#message").value

    this.state.SignalRConnection.invoke("SendMessage", sender, receiver, message)
    .catch(err => {
      console.log("=================== ERROR SENDING CHAT ================")
      console.log(err)
    })

  }



  render(){
    return (<div>

        <div>
            <h2>Chat Test</h2>
            <div>
                <ul id="messageList"></ul>
            </div>
        </div>
        <div>
            <div>Sender: <input type="text" id="sender"/></div>
            <div>Receiver: <input type="text" id="receiver"/></div>
            <div>Message: <input type="text" id="message" /></div>
            <div><input type="button" id="sendMessage" value="Send" onClick={() => {this.sendMessage()}}/></div>
        </div>

    </div>)
  }

}

// function App() {
//   return (
//     <div className="App">
//       <header className="App-header">
//         <img src={logo} className="App-logo" alt="logo" />
//         <p>
//           Edit <code>src/App.js</code> and save to reload.
//         </p>
//         <a
//           className="App-link"
//           href="https://reactjs.org"
//           target="_blank"
//           rel="noopener noreferrer"
//         >
//           Learn React
//         </a>
//       </header>
//     </div>
//   );
// }

export default App;
