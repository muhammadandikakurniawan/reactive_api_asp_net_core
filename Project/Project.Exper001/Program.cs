﻿using System;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Threading;

namespace Project.Exper001
{
    class ChatModel
    {
        public string Sender { get; set; }
        public string Receiver { get; set; }
        public string Message { get; set; }
    }
    class Program
    {
        static void Main(string[] args)
        {

                Subject<ChatModel> subjectChatModel = new Subject<ChatModel>();

                subjectChatModel.Subscribe((data) =>
                {
                    Console.WriteLine("================================================");
                    Console.WriteLine($"From : '{data.Sender}'");
                    Console.WriteLine($"To : '{data.Receiver}'");
                    Console.WriteLine($"Message : '{data.Message}'");
                });


                for(int i = 0; i < 10; i++)
                {
                    Thread.Sleep(2000);
                    var dataChat = new ChatModel
                    {
                        Sender = "User-"+i,
                        Receiver = "User-"+(i+1),
                        Message = "hi User-" + (i + 1) +" this is User-"+i
                    };

                    subjectChatModel.OnNext(dataChat);

                    
                }





        }
    }
}
