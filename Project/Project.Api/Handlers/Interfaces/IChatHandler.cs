﻿using Project.Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Project.Api.Handlers.Interfaces
{
    public interface IChatEventHandler
    {
        void Publish(ChatMessageReceivedEvent param);
        void Subcribe(string name, Action<ChatMessageReceivedEvent> action);
        void Subcribe(string name, Func<ChatMessageReceivedEvent, bool> filter, Action<ChatMessageReceivedEvent> action);
    }
}
