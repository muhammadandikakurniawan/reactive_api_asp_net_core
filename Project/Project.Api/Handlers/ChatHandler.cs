﻿
using Project.Api.Handlers.Interfaces;
using Project.Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Subjects;
using System.Reactive.Linq;
using System.Threading.Tasks;

namespace Project.Api.Handlers
{
    public class ChatEventHandler : IChatEventHandler, IDisposable
    {
        private readonly Subject<ChatMessageReceivedEvent> _subject;
        private readonly IDictionary<string, IDisposable> _subcriber;

        public ChatEventHandler()
        {
            this._subject = new Subject<ChatMessageReceivedEvent>();
            this._subcriber = new Dictionary<string, IDisposable>();
        }
        public void Publish(ChatMessageReceivedEvent param)
        {
            this._subject.OnNext(param);
        }

        public void Subcribe(string name, Action<ChatMessageReceivedEvent> action)
        {
            if(!this._subcriber.ContainsKey(name))
            {
                this._subcriber.Add(name, this._subject.Subscribe(action));
            }
        }

        public void Subcribe(string name, Func<ChatMessageReceivedEvent,bool> filter, Action<ChatMessageReceivedEvent> action)
        {
            if (!this._subcriber.ContainsKey(name))
            {
                this._subcriber.Add(name, this._subject.Where(filter).Subscribe(action));
            }
        }

        public void Dispose()
        {
            if(this._subject != null)
            {
                this._subject.Dispose();
            }

            foreach (var sub in this._subcriber)
            {
                sub.Value.Dispose();
            }
        }
    }
}
