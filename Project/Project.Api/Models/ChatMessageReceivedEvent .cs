﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Project.Api.Models
{
    public class ChatMessageReceivedEvent
    {
        public ChatMessageReceivedEvent() { }

        public ChatMessageReceivedEvent(string sender, string receiver, string msg)
        {
            this.Sender = sender;
            this.Receiver = receiver;
            this.Message = msg;
        }

        public string Sender { get; set; }
        public string Receiver { get; set; }
        public string Message { get; set; }
    }
}
