﻿using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Project.Api.Handlers.Interfaces;
using Project.Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Project.Api.Consumers
{
    public class ChatLogConsumer : BackgroundService
    {
        private readonly IChatEventHandler _chatEventHandler;
        private readonly ILogger<ChatLogConsumer> _logger;

        public ChatLogConsumer(IChatEventHandler chatEventHandler, ILogger<ChatLogConsumer> logger)
        {
            this._chatEventHandler = chatEventHandler;
            this._logger = logger;
        }


        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {

            //DataModel datas = repository.getAll()
            //prosesData(datas)



            await Task.Run(() => { this._chatEventHandler.Subcribe(nameof(ChatLogConsumer), insertLoggChat); });

        }

        private void insertLoggChat(ChatMessageReceivedEvent data)
        {
            this._logger.LogInformation($"Send Message : {data.Message} From : {data.Sender} To {data.Receiver}");
        }
    }
}
