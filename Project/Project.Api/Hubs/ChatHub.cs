﻿using Microsoft.AspNetCore.SignalR;
using Project.Api.Handlers.Interfaces;
using Project.Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Project.Api.Hubs
{
    public class ChatHub : Hub
    {
        private readonly IChatEventHandler _chatEventHandler;

        public ChatHub(IChatEventHandler chatEventHandler)
        {
            this._chatEventHandler = chatEventHandler;
        }

        public async Task SendMessage(string sender, string receiver, string message)
        {
            await Clients.All.SendAsync(receiver, sender, receiver, message);

            _chatEventHandler.Publish(new ChatMessageReceivedEvent(sender,receiver,message));
        }
    }
}
